const express = require('express')
const bookController = require('../controllers/book_controller')
const router = express.Router();

//get all books
/**
  * @swagger
  * /book/:
  *  get:
  *   summary: get all books in lib
  *   tags:
  *   - Book
  *   description: all books
  *   requestBody:
  *    content:
  *     applibookion/json:
  *      schema:
  *       $ref: '#/definitions/Book'
  *   responses:
  *    200:
  *     description: get all books succesfully
  *    500:
  *     description: fail to get all books
  */
router.get('/', bookController.getAllBooks)

//get a books with id
/**
  * @swagger
  * /book/{id}:
  *  get:
  *   summary: get a book using id
  *   tags:
  *     - Book
  *   description: get a book
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a book"
  *     required: true
  *     type: "string"
  *   responses:
  *    200:
  *     description: get a book succesfully
  *    500:
  *     description: fail to get a book
  */
router.get('/:id', bookController.getBookById)

//getBookByGroup
/**
  * @swagger
  * /book/group:
  *  get:
  *   summary: get all group book
  *   tags:
  *     - Book
  *   description: get all book in group
  *   
  *   parameters:
  *   - name: "group"
  *     in: "path"
  *     description: "name group book"
  *     required: true
  *     type: "string"
  *   responses:
  *    200:
  *     description: get a book succesfully
  *    500:
  *     description: fail to get a book
  */
router.get('/group/all', bookController.getAllGroupBook)

//getBookByGroup
/**
  * @swagger
  * /book/group/{group}:
  *  get:
  *   summary: get a book using id
  *   tags:
  *     - Book
  *   description: get all book in group
  *   
  *   parameters:
  *   - name: "group"
  *     in: "path"
  *     description: "name group book"
  *     required: true
  *     type: "string"
  *   responses:
  *    200:
  *     description: get a book succesfully
  *    500:
  *     description: fail to get a book
  */
router.get('/group/groupName/:groupName', bookController.getBookByGroup)

//thue 1 cuon sach

//post save a book to db
/**
  * @swagger
  * /book/store:
  *  post:
  *   summary: save a new book to db
  *   tags:
  *     - Book
  *   description: post a new book
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: book object that need added to store
  *     schema:
  *       $ref: '#/definitions/Book'
  *   responses:
  *    200:
  *     description: Save a new book successfully
  *    500:
  *     description: fail to save a book
  */
router.post('/store', bookController.saveBook)

//edit info a dog id
/**
  * @swagger
  * /book/edit:
  *  put:
  *   summary: change name of a book
  *   description: change name of bookId
  *   tags:
  *     - Book
  *   parameters:
  *   - in: body
  *     name: body
  *     description: book object that need change name
  *     schema:
  *       $ref: '#/definitions/Stock'
  *   responses:
  *    200:
  *     description: Change name a new book successfully
  *    500:
  *     description: fail to save a book
  */
router.put('/edit', bookController.updateBook)

//delete a book with id
/**
  * @swagger
  * /book/delete/{id}:
  *  delete:
  *   summary: delete a book
  *   description: delete a bookId
  *   tags:
  *     - Book
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet book that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a book
  *    500:
  *     description: fail to delete a book
  */
router.delete('/delete/:id', bookController.deleteBook)

module.exports = router;