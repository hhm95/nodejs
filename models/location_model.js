var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Location:
 *     type: object
 *     properties:
 *       location:
 *         type: string
 *         description: vi tri gia sach
 *         example: 'A00'
 *       description:
 *         type: string
 *         description: mo ta vi tri gia sach
 *         example: '1000'
 */

var locationSchema= new mongoose.Schema({
    
    location: String,
    description: String,
},{ versionKey: false });
var Location=mongoose.model('Location',locationSchema,'locations');
module.exports=Location;