const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000
const initRouter = require('./routes/router')
const connectionDB = require('./config/connect_DB')

connectionDB();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

initRouter(app);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})