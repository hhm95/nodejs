const express = require('express')
const publisherController = require('../controllers/publisher_controller')
const router = express.Router();

//get all cats
/**
  * @swagger
  * /publisher/:
  *  get:
  *   summary: get all publishers in lib
  *   tags:
  *   - Publisher
  *   description: all publishers
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/Publisher'
  *   responses:
  *    200:
  *     description: get all publishers succesfully
  *    500:
  *     description: fail to get all publishers
  */
router.get('/',publisherController.getAllPublishers)

//get all books of publisher
/**
  * @swagger
  * /publisher/{name}:
  *  get:
  *   summary: get all books of publisher
  *   tags:
  *     - Publisher
  *   description: get all books
  *   
  *   parameters:
  *   - name: "name"
  *     in: "path"
  *     description: "name of publisher"
  *     required: true
  *     type: "string"
  *   responses:
  *    200:
  *     description: get by publisher succesfully
  *    500:
  *     description: fail to get by publisher
  */
router.get('/:name',publisherController.getPublisherByName)

//post save a cat to db
/**
  * @swagger
  * /publisher/store:
  *  post:
  *   summary: save a new cat to db
  *   tags:
  *     - Publisher
  *   description: post a new cat
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: cat object that need added to store
  *     schema:
  *       $ref: '#/definitions/Publisher'
  *   responses:
  *    200:
  *     description: Save a new cat successfully
  *    500:
  *     description: fail to save a cat
  */
router.post('/store',publisherController.savePublisher)

//edit info a dog id
/**
  * @swagger
  * /publisher/edit:
  *  put:
  *   summary: change name of a cat
  *   description: change name of catId
  *   tags:
  *     - Publisher
  *   parameters:
  *   - in: body
  *     name: body
  *     description: cat object that need change name
  *     schema:
  *       $ref: '#/definitions/Publisher'
  *   responses:
  *    200:
  *     description: Change name a new cat successfully
  *    500:
  *     description: fail to save a cat
  */
router.put('/edit',publisherController.updatePublisher)

//delete a cat with id
/**
  * @swagger
  * /publisher/delete/{id}:
  *  delete:
  *   summary: delete a cat
  *   description: delete a catId
  *   tags:
  *     - Publisher
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet cat that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a cat
  *    500:
  *     description: fail to delete a cat
  */
router.delete('/delete/:id',publisherController.deletePublisher)

module.exports = router;