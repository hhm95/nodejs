const express = require('express')
const initController = require('../controllers/init_controller')
const router = express.Router();

//get all dogs
router.get('/',initController.init)

module.exports = router;