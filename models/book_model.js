var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Book:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *         description: private name of pet
 *         example: 'Giai tich 1'
 *       group:
 *         type: string
 *         description: private name of pet
 *         example: 'GT'
 *       publisherId:
 *         type: string
 *         description: private name of pet
 *         example: '60f14ecf88b0174c090bbc17'
 *       authorId:
 *         type: string
 *         description: private name of pet
 *         example: '60f146ac88b0174c090bbc0b'
 *       year:
 *         type: string
 *         description: private name of pet
 *         example: '2000'
 *       version:
 *         type: string
 *         description: private name of pet
 *         example: '1'
 *       price:
 *         type: string
 *         description: private name of pet
 *         example: '100200'
 *       language:
 *         type: string
 *         description: private name of pet
 *         example: 'English'
 *       locationId:
 *         type: string
 *         description: private name of pet
 *         example: 'A00'
 *       categoryId:
 *         type: string
 *         description: private name of pet
 *         example: '1'
 *       description:
 *         type: string
 *         description: private name of pet
 *         example: 'ABCD'
 *       status:
 *         type: string
 *         description: private name of pet
 *         example: 'avaiable'
 *   Books:
 *     type: array
 *     items:
 *       $ref: '#/definitions/Book'
 */

var bookSchema= new mongoose.Schema({
    name: String,
    group: String,
    publisherId: String,
    authorId: String,
    year: String,
    version: String,
    price: String,
    language: String,
    locationId: String,
    categoryId: String,
    description: String,
    status: String
},{ versionKey: false });
var Book=mongoose.model('Book',bookSchema,'books');
module.exports=Book;