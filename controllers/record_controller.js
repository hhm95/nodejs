var Record = require('../models/record_model');
var ObjectId = require('objectid')
var Book = require('../models/book_model');
var Student = require('../models/student_model');
class recordController {

    getAllRecords(req, res) {
        Record.find({}, function (err, authors) {
            if (err) {
                console.log(err)
            }
            res.send(authors);
        });

    }

    //get a dogs with id
    getRecordById(req, res) {
        const id = req.params.id;
        res.send('this is a Record information' + id)
    }

    //post save a dog to db
    async saveRecord(req, res) {

        const newRecord = req.body;

        const idBook = req.body.book;
        const startDate = req.body.dateStart;
        const endDate = req.body.dateEnd;
        const user = req.body.user;
        const nameStudent = req.body.student;

        if (!ObjectId.isValid(idBook)) {
            res.status(400).json('Invalid idBook')
            return;
        }

        var o_id = new ObjectId(idBook);

        var book = await Book.findOne({ "_id": o_id })
        if (book == null) {
            res.status(400).json('not found book')
            return;
        }
        if (book.status == "lent") {
            //neu cuon sach dang cho thue
            res.status(400).json('Book lent')
            return;
        }
        book.status = "lent";
        var afterBook = await book.save();
        var student = await Student.findOne({ "username": nameStudent })
        if (student == null) {
            res.status(400).json('not found student')
            return;
        }
        var oldBorrowing = student.borrowing;
        console.log(oldBorrowing);

        oldBorrowing.push(idBook)
        console.log(oldBorrowing);
        student.borrowing = oldBorrowing;
        var afterStudent = await student.save();

        var book = await Record.create(newRecord);
        res.send(afterStudent);
    }

    //edit info a dog id
    updateRecord(req, res) {
        res.send(req.body)
    }

    //delete a dog with id
    deleteRecord(req, res) {
        const id = req.params.id;
        res.send('Delete a Record to db' + id)
    }

}

module.exports = new recordController;

