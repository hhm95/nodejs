const express = require('express')
const categoryController = require('../controllers/category_controller')
const router = express.Router();

//get all cats
/**
  * @swagger
  * /category/:
  *  get:
  *   summary: get all categorys in lib
  *   tags:
  *   - Category
  *   description: all categorys
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/Category'
  *   responses:
  *    200:
  *     description: get all categorys succesfully
  *    500:
  *     description: fail to get all categorys
  */
router.get('/',categoryController.getAllCategorys)

//get a cats with id
/**
  * @swagger
  * /category/{id}:
  *  get:
  *   summary: get a cat using id
  *   tags:
  *     - Category
  *   description: get a cat
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a cat"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: get a cat succesfully
  *    500:
  *     description: fail to get a cat
  */
router.get('/:id',categoryController.getCategoryById)

//post save a cat to db
/**
  * @swagger
  * /category/store:
  *  post:
  *   summary: save a new cat to db
  *   tags:
  *     - Category
  *   description: post a new cat
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: cat object that need added to store
  *     schema:
  *       $ref: '#/definitions/Category'
  *   responses:
  *    200:
  *     description: Save a new cat successfully
  *    500:
  *     description: fail to save a cat
  */
router.post('/store',categoryController.saveCategory)

//edit info a dog id
/**
  * @swagger
  * /category/edit:
  *  put:
  *   summary: change name of a cat
  *   description: change name of catId
  *   tags:
  *     - Category
  *   parameters:
  *   - in: body
  *     name: body
  *     description: cat object that need change name
  *     schema:
  *       $ref: '#/definitions/Category'
  *   responses:
  *    200:
  *     description: Change name a new cat successfully
  *    500:
  *     description: fail to save a cat
  */
router.put('/edit',categoryController.updateCategory)

//delete a cat with id
/**
  * @swagger
  * /category/delete/{id}:
  *  delete:
  *   summary: delete a cat
  *   description: delete a catId
  *   tags:
  *     - Category
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet cat that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a cat
  *    500:
  *     description: fail to delete a cat
  */
router.delete('/delete/:id',categoryController.deleteCategory)

module.exports = router;