const con = require('../config/connect_DB')
var User=require('../models/user_model');

class userController{

    getAllUsers(req,res){
        User.find({}, function(err,authors){
            if(err){
                console.log(err)
            }
            res.send(authors);
        });

    }

    //get a dogs with id
    getUserById(req,res){
        const id = req.params.id;
        res.send('this is a User information' + id)
    }

    //post save a dog to db
    saveUser(req,res){
        res.send(req.body)
    }

    //edit info a dog id
    updateUser(req,res){
        res.send(req.body)
    }

    //delete a dog with id
    deleteUser(req,res){
        const id = req.params.id;
        res.send('Delete a User to db' + id)
    }

}

module.exports = new userController;

