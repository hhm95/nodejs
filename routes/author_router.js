const express = require('express')
const authorController = require('../controllers/author_controller')
const router = express.Router();

//get all dogs
/**
  * @swagger
  * /author/:
  *  get:
  *   summary: get all author
  *   tags:
  *   - Author
  *   description: all author
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/Author'
  *   responses:
  *    200:
  *     description: get all tac gia
  *    500:
  *     description: fail to get all 
  */
router.get('/',authorController.getAllAuthors)

//get a author with id
/**
  * @swagger
  * /author/{name}:
  *  get:
  *   summary: get a author using id
  *   tags:
  *     - Author
  *   description: get a author
  *   
  *   parameters:
  *   - name: "name"
  *     in: "path"
  *     description: "ten của tác giả"
  *     required: true
  *     type: "string"
  *   responses:
  *    200:
  *     description: get a author succesfully
  *    500:
  *     description: fail to get 
  */
router.get('/:id',authorController.getAuthorById)

//post save a author to db
/**
  * @swagger
  * /author/store:
  *  post:
  *   summary: save a new author to db
  *   tags:
  *     - Author
  *   description: post a new dog
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: author object that need added to store
  *     schema:
  *       $ref: '#/definitions/Author'
  *   responses:
  *    200:
  *     description: Save a new author successfully
  *    500:
  *     description: fail to save a author
  */
router.post('/store',authorController.saveAuthor)

//edit info a author',authorController.updateauthor)
/**
  * @swagger
  * /author/edit:
  *  put:
  *   summary: change name of a author
  *   description: change name of author
  *   tags:
  *     - Author
  *   parameters:
  *   - in: body
  *     name: body
  *     description: author object that need change name
  *     schema:
  *       $ref: '#/definitions/Stock'
  *   responses:
  *    200:
  *     description: Change name a new author successfully
  *    500:
  *     description: fail to save a author
  */
//edit info a dog id
router.put('/edit',authorController.updateAuthor)
//delete a author with id

/**
  * @swagger
  * /author/delete/{id}:
  *  delete:
  *   summary: delete a author
  *   description: delete a author
  *   tags:
  *     - Author
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a author
  *    500:
  *     description: fail to delete a author
  */
router.delete('/delete/:id',authorController.deleteAuthor)

module.exports = router;