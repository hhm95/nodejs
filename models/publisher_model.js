var mongoose = require('mongoose');


/**
 * @swagger
 * definitions:
 *   Publisher:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *         description: nha xuat ban
 *         example: 'NXB Kim Dong'
 *       address:
 *         type: string
 *         description: dia chi nha xuat ban
 *         example: 'Dia chi X Y Z'
 *       website:
 *         type: string
 *         description: website nha xuat ban
 *         example: 'abcd.com'
 *       phone:
 *         type: string
 *         description: sdt nha xuat ban
 *         example: '03112345678'
 */

var publisherSchema = new mongoose.Schema({
    name: String,
    address: String,
    website: String,
    phone: String
},{ versionKey: false });
var Publisher = mongoose.model('Publisher', publisherSchema, 'publishers');
module.exports = Publisher;