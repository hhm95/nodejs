
var Book = require('../models/book_model');
var ObjectId = require('objectid')



class bookController {

    async getAllBooks(req, res) {

        // Book.find({}, function (err, books) {
        //     if (err) {
        //         console.log(err)
        //     }
        //     res.send(books);
        // });
        var books = await Book.find();
        if (books == null) {
            res.status(500).json();
            return;
        }
        res.status(200).json(books);
    }


    //get a dogs with id
    async getBookById(req, res) {
        const id = req.params.id;
        // console.log(id)

        if (ObjectId.isValid(id)) {
            var o_id = new ObjectId(id);
            // Book.find({ "_id": o_id }, function (err, books) {
            //     if (err) {
            //         console.log(err)
            //     }
            //     res.send(books);
            // });
            var book = await Book.findOne({ "_id": o_id })
            if (book == null) {
                res.status(400).json()
                return;
            }
            res.status(200).json(book)
        } else {

            res.status(404).json('id not valid')
        }

    }
    //getAllGroupBook
    async getAllGroupBook(req, res) {

        console.log('get all group')

        // if(ObjectId.isValid(id)){

        // const allGroup = await Book.find({},{group:1,_id:0})
        // if(allGroup == null){
        //     res.status(400).json()
        //     return;
        // }
        // var nameArray = allGroup.map(function (el) { return el.group; });
        // console.log(nameArray);

        // var occurrences = nameArray.reduce(function(obj, item) {
        // obj[item] = (obj[item] || 0) + 1;
        // return obj;
        // }, {});

        // console.log(occurrences);   
        // res.status(200).json(occurrences)
        // return;

        const allGroup = await Book.distinct('group');
        var dataOut = [];
        for (const item of allGroup) {
            console.log(item)
            var data = await Book.find({ "group": item });
            console.log(data.length)
            dataOut.push(
                {
                    "nameGroup": item,
                    "count": data.length
                }
            )
        }

        res.send(dataOut)
    }

    //get by group
    async getBookByGroup(req, res) {

        const group = req.params.groupName;
        if (group == null) {
            getAllGroupBook(req, res);
            return;
        }


        console.log(group)
        // if(ObjectId.isValid(id)){

        var book = await Book.find({ "group": group })
        if (book == null) {
            res.status(400).json()
            return;
        }
        const leng_group = book.length;
        console.log(leng_group)
        const data = {
            "groupName": group,
            "numbers": leng_group
        }
        res.status(200).json(book)
        return;
    }

    //student muon 1 cuon sach
    async rentABook(req, res) {


        const user = "manhhh2";
        const idBook = req.params.idBook;
        const startDate = req.params.startDate;
        const endDate = req.params.endDate;



    }

    //post save a dog to db
    async saveBook(req, res) {
        res.send(req.body)

        const newBook = req.body;

        var book = await Book.create(newBook);
        res.status(200).json(book)
    }

    //edit info a dog id
    updateBook(req, res) {
        res.send(req.body)
    }

    //delete a dog with id
    deleteBook(req, res) {
        const id = req.params.id;
        res.send('Delete a Cat to db: ' + id)
    }

}

module.exports = new bookController;

