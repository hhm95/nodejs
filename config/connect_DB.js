var mongoose = require('mongoose');

let connectDB = () => {
  mongoose.connect('mongodb://userName:passWord@localhost:27017/thuvienDB', 
  { useNewUrlParser: true, useUnifiedTopology: true },(err,result) => {
    if(err)
      console.log("Connect DB err "+err)
    else
      console.log("Connected MongoDB")
  } );
};

module.exports = connectDB;