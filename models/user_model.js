var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *         description: name
 *         example: 'manhhh2'
 *       password:
 *         type: string
 *         description: pass
 *         example: 'Abcd@1234'
 *       type:
 *         type: string
 *         description: role
 *         example: 'user'
 */

var authorSchema= new mongoose.Schema({
    username: String,
    password: String,
    type: String,
},{ versionKey: false });
var User=mongoose.model('User',authorSchema,'users');
module.exports=User;