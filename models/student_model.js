var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Student:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *         description: ten sinh vien
 *         example: 'Hoang Huu Manh'
 *       password:
 *         type: string
 *         description: mat khau
 *         example: 'Abcd@1234'
 *       mssv:
 *         type: string
 *         description: ma so sinh vien
 *         example: '20132512'
 *       class:
 *         type: string
 *         description: lop hoc
 *         example: 'BK 2.02'
 *       email:
 *         type: string
 *         description: email
 *         example: 'abcd@gmail.com'
 *       status:
 *         type: string
 *         description: trang thai
 *         example: 'active'
 *       role:
 *         type: string
 *         description: role
 *         example: '00'
 */

var studentSchema= new mongoose.Schema({
    username: String,
    password: String,
    mssv: String,
    class: String,
    email: String,
    status: String,
    borrowing: Array,
    role: String,
},{ versionKey: false });
var Student=mongoose.model('Student',studentSchema,'students');
module.exports=Student;