const express = require('express')
const recordController = require('../controllers/record_controller')
const router = express.Router();

//get all dogs
/**
  * @swagger
  * /record/:
  *  get:
  *   summary: get all record
  *   tags:
  *   - record
  *   description: all record
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/Stock'
  *   responses:
  *    200:
  *     description: get all record succesfully
  *    500:
  *     description: fail to get all record
  */
router.get('/',recordController.getAllRecords)

//get a record with id
/**
  * @swagger
  * /record/{id}:
  *  get:
  *   summary: get a record using id
  *   tags:
  *     - record
  *   description: get a record
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: get a record succesfully
  *    500:
  *     description: fail to get a record
  */
router.get('/:id',recordController.getRecordById)

//post save a record to db
/**
  * @swagger
  * /record/store:
  *  post:
  *   summary: save a new record to db
  *   tags:
  *     - record
  *   description: post a new dog
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: record object that need added to store
  *     schema:
  *       $ref: '#/definitions/Record'
  *   responses:
  *    200:
  *     description: Save a new record successfully
  *    500:
  *     description: fail to save a record
  */
router.post('/store',recordController.saveRecord)

//edit info a record',recordController.updaterecord)
/**
  * @swagger
  * /record/edit:
  *  put:
  *   summary: change name of a record
  *   description: change name of record
  *   tags:
  *     - record
  *   parameters:
  *   - in: body
  *     name: body
  *     description: record object that need change name
  *     schema:
  *       $ref: '#/definitions/Stock'
  *   responses:
  *    200:
  *     description: Change name a new record successfully
  *    500:
  *     description: fail to save a record
  */
//edit info a dog id
router.put('/edit',recordController.updateRecord)
//delete a record with id

/**
  * @swagger
  * /record/delete/{id}:
  *  delete:
  *   summary: delete a record
  *   description: delete a record
  *   tags:
  *     - record
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a record
  *    500:
  *     description: fail to delete a record
  */
router.delete('/delete/:id',recordController.deleteRecord)

module.exports = router;