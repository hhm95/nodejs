const express = require('express')
const userController = require('../controllers/user_controller')
const router = express.Router();

const swagger = require('../controllers/swagger')


/**
  * @swagger
  * /user/:
  *  get:
  *   summary: get all users
  *   tags:
  *   - User
  *   description: all users
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/User'
  *   responses:
  *    200:
  *     description: get all users succesfully
  *    500:
  *     description: fail to get all users
  */
//get all users
router.get('/',userController.getAllUsers)


/**
  * @swagger
  * /user/{id}:
  *  get:
  *   summary: get a user using id
  *   tags:
  *     - User
  *   description: get a user
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: get a user succesfully
  *    500:
  *     description: fail to get a user
  */
//get a users with id
router.get('/:id',userController.getUserById)


//post save a user to db

/**
  * @swagger
  * /user/store:
  *  post:
  *   summary: save a new user to db
  *   tags:
  *     - User
  *   description: post a new user
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: user object that need added to store
  *     schema:
  *       $ref: '#/definitions/User'
  *   responses:
  *    200:
  *     description: Save a new user successfully
  *    500:
  *     description: fail to save a user
  */
router.post('/store',userController.saveUser)

// /**
//   * @swagger
//   * /user/{id}:
//   *  put:
//   *   summary: change name of a user
//   *   description: change name of userId
//   *   
//   *   parameters:
//   *   - name: "id"
//   *     in: "path"
//   *     description: "id of a pet"
//   *     required: true
//   *     type: "integer"
//   *     format: "int64"
//   *   - name: "name"
//   *     in: "path"
//   *     description: "new name of perId"
//   *     type: "string"
//   *     required: true
//   *   responses:
//   *    200:
//   *     description: change name succesfully
//   *    500:
//   *     description: fail to get a user
//   */
/**
  * @swagger
  * /user/edit:
  *  put:
  *   summary: change name of a user
  *   description: change name of userId
  *   tags:
  *     - User
  *   parameters:
  *   - in: body
  *     name: body
  *     description: user object that need change name
  *     schema:
  *       $ref: '#/definitions/User'
  *   responses:
  *    200:
  *     description: Change name a new user successfully
  *    500:
  *     description: fail to save a user
  */
//edit info a user id
router.put('/edit',userController.updateUser)

//delete a user with id
// router.get('/delete/:id',userController.deleteuser)

/**
  * @swagger
  * /user/delete/{id}:
  *  delete:
  *   summary: delete a user
  *   description: delete a userId
  *   tags:
  *     - User
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a user
  *    500:
  *     description: fail to delete a user
  */
router.delete('/delete/:id',userController.deleteUser)

module.exports = router;