const express = require('express')
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const router = express.Router();

const options = {
    swaggerDefinition: {
      info: {
        title: 'REST - Swagger',
        version: '1.0.0',
        description: 'REST API with Swagger doc',
        contact: {
            name: 'Hoang Huu Manh',
            email: 'hoanghuuanh19991@gmail.com',
        },
      },
      tags: [
        {
          name: 'My Farm',
          description: 'All apis with my farm',
        },
      ],
      schemes: ['http'],
      host: 'localhost:3000',
    },
    apis: ['./routes/*.js','./models/*.js'],
  };

const swaggerDocs = swaggerJSDoc(options);

// router.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocs));


module.exports = swaggerDocs;
