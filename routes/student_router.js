const express = require('express')
const studentController = require('../controllers/student_controller')
const router = express.Router();

//get all students
/**
  * @swagger
  * /student/:
  *  get:
  *   summary: get all student
  *   tags:
  *   - student
  *   description: all student
  *   requestBody:
  *    content:
  *     application/json:
  *      schema:
  *       $ref: '#/definitions/Student'
  *   responses:
  *    200:
  *     description: get all student succesfully
  *    500:
  *     description: fail to get all student
  */
router.get('/',studentController.getAllStudents)

//get a student with id
/**
  * @swagger
  * /student/{id}:
  *  get:
  *   summary: get a student using id
  *   tags:
  *     - student
  *   description: get a student
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: get a student succesfully
  *    500:
  *     description: fail to get a student
  */
router.get('/:id',studentController.getStudentById)

//post save a student to db
/**
  * @swagger
  * /student/store:
  *  post:
  *   summary: save a new student to db
  *   tags:
  *     - student
  *   description: post a new student
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: student object that need added to store
  *     schema:
  *       $ref: '#/definitions/Student'
  *   responses:
  *    200:
  *     description: Save a new student successfully
  *    500:
  *     description: fail to save a student
  */
router.post('/store',studentController.saveStudent)

//edit info a student',studentController.updatestudent)
/**
  * @swagger
  * /student/edit:
  *  put:
  *   summary: change name of a student
  *   description: change name of student
  *   tags:
  *     - student
  *   parameters:
  *   - in: body
  *     name: body
  *     description: student object that need change name
  *     schema:
  *       $ref: '#/definitions/Student'
  *   responses:
  *    200:
  *     description: Change name a new student successfully
  *    500:
  *     description: fail to save a student
  */
//edit info a student id
router.put('/edit',studentController.updateStudent)
//delete a student with id

/**
  * @swagger
  * /student/delete/{id}:
  *  delete:
  *   summary: delete a student
  *   description: delete a student
  *   tags:
  *     - student
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a student
  *    500:
  *     description: fail to delete a student
  */
router.delete('/delete/:id',studentController.deleteStudent)

module.exports = router;