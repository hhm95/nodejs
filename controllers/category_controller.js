const con = require('../config/connect_DB')
var Category=require('../models/category_model');

class categoryController{

    async getAllCategorys(req,res){
        // Category.find({}, function(err,authors){
        //     if(err){
        //         console.log(err)
        //     }
        //     res.send(authors);
        // });
        var categories = await Category.find();
        res.status(200).json(categories)

    }

    //get a dogs with id
    getCategoryById(req,res){
        const id = req.params.id;
        res.send('this is a Category information' + id)
    }

    //post save a dog to db
    saveCategory(req,res){
        res.send(req.body)
    }

    //edit info a dog id
    updateCategory(req,res){
        res.send(req.body)
    }

    //delete a dog with id
    deleteCategory(req,res){
        const id = req.params.id;
        res.send('Delete a Category to db' + id)
    }

}

module.exports = new categoryController;

