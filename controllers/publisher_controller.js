var Publisher = require('../models/publisher_model');
var Book = require('../models/book_model');
var ObjectId = require('objectid')

class publisherController {

    getAllPublishers(req, res) {

        Publisher.find({}, function (err, authors) {
            if (err) {
                console.log(err)
            }
            res.send(authors);
        });
    }

    //get a Publishers with id
    async getPublisherByName(req, res) {
        const namePublisher = req.params.name;
        console.log(namePublisher)

        var publisher = await Publisher.findOne({ "name": namePublisher },);
        if (publisher == null) {
            res.status(500).json('ko thay nha xuat ban')
            return;
        }
        var books = await Book.find({ "publisherId": publisher._id });
        if (books == null || books.length == 0) {
            res.status(500).json('nxh ko co cuon sach nao')
            return;
        }
        res.status(200).json(books);


        //Promise
        // Publisher.findOne({ "name": namePublisher }).then(publisher => {
        //     if (publisher == null) {
        //         res.status(500).json('ko thay nha xuat ban')
        //         return;
        //     }


        //     console.log(publisher)
        //     var idPublisher = publisher._id
        //     console.log(idPublisher)


        //     Book.find({ "publisherId": idPublisher }).then(books => {
        //         if (books == null || books.length == 0) {
        //             res.status(404).json('co nxh nhung ko co sach nao')
        //             return;
        //         }
        //         res.status(200).json(books);
        //     }).catch(err => {
        //         console.log(err)
        //         res.status(404).json('loi tim sach khi co nha xuat ban')
        //     })

        // }).catch(err => {
        //     console.log(err)
        //     res.status(404).json('err khi tim nha xuat ban')
        // })



        ///call back
        // Publisher.findOne({ "name": namePublisher }, function (err, publisher) {
        //     if (err || (publisher == null)) {
        //         console.log(err)
        //         res.status(500).json([])
        //         return;
        //     }

        //     console.log(publisher)
        //     var idPublisher = publisher._id
        //     console.log(idPublisher)
        //     Book.find({ "publisherId": idPublisher }, function (err, books) {
        //         if (err) {
        //             console.log(err)
        //             return;
        //         }
        //         res.status(200).json(books);
        //     });


        // });


    }


    savePublisher(req, res) {
        const newPublisher = req.body;
        res.send(newPublisher)
    }

    //edit info a Publisher id
    updatePublisher(req, res) {
        res.send(req.body)
    }

    //delete a Publisher with id
    deletePublisher(req, res) {
        const id = req.params.id;
        console.log(id)
        res.send('Delete a PublisherID to db: ' + id)
    }

}

module.exports = new publisherController;