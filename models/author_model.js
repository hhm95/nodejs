var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Author:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *         description: private name of pet
 *         example: 'Hoang Huu Manh'
 *       year:
 *         type: string
 *         description: private name of pet
 *         example: '2000'
 *       nickname:
 *         type: string
 *         description: private name of pet
 *         example: 'xxxyyyzzz'
 */

var authorSchema= new mongoose.Schema({
    name: String,
    year: String,
    nickname: String
},{ versionKey: false });
var Author=mongoose.model('Author',authorSchema,'authors');
module.exports=Author;