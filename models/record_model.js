var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Record:
 *     type: object
 *     properties:
 *       book:
 *         type: string
 *         description: idBook
 *         example: 'idBook'
 *       student:
 *         type: string
 *         description: ten nguoi muon
 *         example: 'minh'
 *       dateStart:
 *         type: Date
 *         description: ngay bat dau muon
 *         example: '2021-07-28'
 *       dateEnd:
 *         type: Date
 *         description: ngay tra
 *         example: '2021-08-31'
 *       user:
 *         type: string
 *         description: user cho muon
 *         example: 'manhhh2'
 *       status:
 *         type: string
 *         description: trang thai
 *         example: '0'
 */

var recordSchema= new mongoose.Schema({
    book: String,
    student: String,
    dateStart: Date,
    dateEnd: Date,
    user: String,
    status: String
},{ versionKey: false });
var Record=mongoose.model('Record',recordSchema,'records');
module.exports=Record;