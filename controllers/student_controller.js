const con = require('../config/connect_DB')
var Student=require('../models/student_model');

class studentController{

    getAllStudents(req,res){
        Student.find({}, function(err,authors){
            if(err){
                console.log(err)
            }
            res.send(authors);
        });

    }

    //get a dogs with id
    getStudentById(req,res){
        const id = req.params.id;
        res.send('this is a Student information' + id)
    }

    //post save a dog to db
    saveStudent(req,res){
        res.send(req.body)
    }

    //edit info a dog id
    updateStudent(req,res){
        res.send(req.body)
    }

    //delete a dog with id
    deleteStudent(req,res){
        const id = req.params.id;
        res.send('Delete a Student to db' + id)
    }

}

module.exports = new studentController;

