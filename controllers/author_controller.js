var Author=require('../models/author_model');
var Book=require('../models/book_model');
var ObjectId = require('objectid');

class authorController{

    async getAllAuthors(req,res){
        
        //async await
        var authors = await Author.find();
        res.status(200).json(authors);

        //call back

        // Author.find({}, function(err,authors){
        //     if(err){
        //         console.log(err)
        //     }
        //     res.send(authors);
        // });
    }

    //get a dogs with id
    async getAuthorById(req,res){
        const id = req.params.id;
        console.log(id)
        // var o_id = new ObjectId(id);

        var author = await Author.findOne({"name" : id});
        if(author == null){
            res.status(400).json()
            return
        }
        var books =  await Book.find({"authorId" : author._id});
        res.status(200).json(books)
        // Author.findOne({"name" : id}, function(err,author){
        //     if(err){
        //         console.log(err)
        //     }
        //     console.log(author)
        //     var idAuthor = author._id
        //     console.log(idAuthor)
        //     Book.find({"authorId" : idAuthor}, function(err,books){
        //         if(err){
        //             console.log(err)
        //         }
        //         res.send(books);
        //     });
        // });
    }

    //post save a author to db
    async saveAuthor(req,res){
        // res.send(req.body)
        var newAuthor = req.body;
        console.log(newAuthor)
        var author = await Author.create(newAuthor)
        res.status(200).json(author)
    }

    //edit info a dog id
    updateAuthor(req,res){
        res.send(req.body)
    }

    //delete a dog with id
    deleteAuthor(req,res){
        const id = req.params.id;
        res.send('Delete a author to db' + id)
    }

}

module.exports = new authorController;

