const con = require('../config/connect_DB')
var Location=require('../models/location_model');

class locationController{

    getAllLocations(req,res){
        Location.find({}, function(err,authors){
            if(err){
                console.log(err)
            }
            res.send(authors);
        });

    }

    //get a dogs with id
    getLocationById(req,res){
        const id = req.params.id;
        res.send('this is a Location information' + id)
    }

    //post save a dog to db
    saveLocation(req,res){
        res.send(req.body)
    }

    //edit info a dog id
    updateLocation(req,res){
        res.send(req.body)
    }

    //delete a dog with id
    deleteLocation(req,res){
        const id = req.params.id;
        res.send('Delete a Location to db' + id)
    }

}

module.exports = new locationController;

