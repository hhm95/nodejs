const express = require('express')

const bookRouter = require('./book_router')
const authorRouter = require('./author_router')
const userRouter = require('./user_router')
const studentRouter = require('./student_router')
const locationRouter = require('./location_router')
const categoryRouter = require('./category_router')
const recordRouter = require('./record_router')
const publisherRouter = require('./publisher_router')
const initRouter = require('./init_router')

const swaggerDocs = require('../controllers/swagger')
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');



function router(app){
    app.use('/',initRouter)
    app.use('/book',bookRouter);
    app.use('/author',authorRouter);
    app.use('/publisher',publisherRouter);
    app.use('/location',locationRouter);
    app.use('/user',userRouter);
    app.use('/student',studentRouter);
    app.use('/category',categoryRouter);
    app.use('/record',recordRouter);
    app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocs));
    // app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocs));

}


module.exports = router; 