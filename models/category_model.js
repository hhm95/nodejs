var mongoose= require('mongoose');


/**
 * @swagger
 * definitions:
 *   Category:
 *     type: object
 *     properties:
 *       nameCategory:
 *         type: string
 *         description: the loai
 *         example: 'Van hoc'
 *       idParent:
 *         type: string
 *         description: i dont now
 *         example: '123'
 */

var categorySchema= new mongoose.Schema({
    nameCategory: String,
    idParent: String,
},{ versionKey: false });
var Category=mongoose.model('Category',categorySchema,'categories');
module.exports=Category;