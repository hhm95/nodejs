const express = require('express')
const locationController = require('../controllers/location_controller')
const router = express.Router();

//get all locations
/**
  * @swagger
  * /location/:
  *  get:
  *   summary: get all locations in lib
  *   tags:
  *   - Location
  *   description: all locations
  *   requestBody:
  *    content:
  *     applilocationion/json:
  *      schema:
  *       $ref: '#/definitions/Location'
  *   responses:
  *    200:
  *     description: get all locations succesfully
  *    500:
  *     description: fail to get all locations
  */
router.get('/',locationController.getAllLocations)

//get a locations with id
/**
  * @swagger
  * /location/{id}:
  *  get:
  *   summary: get a location using id
  *   tags:
  *     - Location
  *   description: get a location
  *   
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a location"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: get a location succesfully
  *    500:
  *     description: fail to get a location
  */
router.get('/:id',locationController.getLocationById)

//post save a location to db
/**
  * @swagger
  * /location/store:
  *  post:
  *   summary: save a new location to db
  *   tags:
  *     - Location
  *   description: post a new location
  *   
  *   parameters:
  *   - in: body
  *     name: body
  *     description: location object that need added to store
  *     schema:
  *       $ref: '#/definitions/Location'
  *   responses:
  *    200:
  *     description: Save a new location successfully
  *    500:
  *     description: fail to save a location
  */
router.post('/store',locationController.saveLocation)

//edit info a dog id
/**
  * @swagger
  * /location/edit:
  *  put:
  *   summary: change name of a location
  *   description: change name of locationId
  *   tags:
  *     - Location
  *   parameters:
  *   - in: body
  *     name: body
  *     description: location object that need change name
  *     schema:
  *       $ref: '#/definitions/Location'
  *   responses:
  *    200:
  *     description: Change name a new location successfully
  *    500:
  *     description: fail to save a location
  */
router.put('/edit',locationController.updateLocation)

//delete a location with id
/**
  * @swagger
  * /location/delete/{id}:
  *  delete:
  *   summary: delete a location
  *   description: delete a locationId
  *   tags:
  *     - Location
  *   parameters:
  *   - name: "id"
  *     in: "path"
  *     description: "id of a pet location that needed delete"
  *     required: true
  *     type: "integer"
  *     format: "int64"
  *   responses:
  *    200:
  *     description: Succesfully delete a location
  *    500:
  *     description: fail to delete a location
  */
router.delete('/delete/:id',locationController.deleteLocation)

module.exports = router;